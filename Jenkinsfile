import hudson.tasks.test.AbstractTestResultAction
pipeline {
    agent any

    triggers {
        pollSCM('*/2 * * * *')
    }

    environment {
        appServerName = 'iis08'
        APP_SERVER_CREDS = credentials('app-server-cred')
    }

    stages{
        stage('Checkout') {
            steps {
                script {
                    if (env.appServerName == 'iis0x') {
                            error("Environment specific variables HAVE NOT been updated in the Jenkinsfile! Please update them as indicated in the lab instructions to continue.")
                    }
                }
                cleanWs()
                checkout scm
            }
        }
        stage ('Build') {
            steps {
                bat "\"C:/Program Files/dotnet/dotnet.exe\" clean \"${workspace}/eShopOnWeb.sln\""
                bat "\"C:/Program Files/dotnet/dotnet.exe\" build \"${workspace}/eShopOnWeb.sln\""
            }
        }
        stage ('Unit Tests') {
            steps {
                bat returnStatus: true, script: "\"C:/Program Files/dotnet/dotnet.exe\" test \"${workspace}/tests/UnitTests/UnitTests.csproj\" --logger \"trx;LogFileName=unit_tests.xml\" --no-build"
                step([$class: 'MSTestPublisher', testResultsFile:"**/unit_tests*.xml", failOnError: true, keepLongStdio: true])
            }
        }
        stage ('Functional Tests') {
            steps {
                bat returnStatus: true, script: "\"C:/Program Files/dotnet/dotnet.exe\" test \"${workspace}/tests/FunctionalTests/FunctionalTests.csproj\" --logger \"trx;LogFileName=functional_tests.xml\" --no-build"
                step([$class: 'MSTestPublisher', testResultsFile:"**/functional_tests*.xml", failOnError: true, keepLongStdio: true])
            }
        }
        stage ('Integration Tests') {
            steps {
                bat returnStatus: true, script: "\"C:/Program Files/dotnet/dotnet.exe\" test \"${workspace}/tests/IntegrationTests/IntegrationTests.csproj\" --logger \"trx;LogFileName=integration_tests.xml\" --no-build"
                step([$class: 'MSTestPublisher', testResultsFile:"**/integration_tests*.xml", failOnError: true, keepLongStdio: true])
            }
        }
        stage ('Publish') {
            steps {
                bat "\"C:/Program Files/dotnet/dotnet.exe\" publish -c Release -o \"${workspace}/publish\" /p:EnvironmentName=Development \"${workspace}/eShopOnWeb.sln\""
            }
        }
        stage ('Seed Database') {
            steps {
                bat "SET ASPNETCORE_ENVIRONMENT=Development"
                bat "\"C:/Program Files/dotnet/dotnet.exe\" ef database drop -f -c appidentitydbcontext --configuration Release -p \"${workspace}/src/Infrastructure/Infrastructure.csproj\" -s \"${workspace}/src/Web/Web.csproj\""
                bat "\"C:/Program Files/dotnet/dotnet.exe\" ef database drop -f -c catalogcontext --configuration Release -p \"${workspace}/src/Infrastructure/Infrastructure.csproj\" -s \"${workspace}/src/Web/Web.csproj\""
                bat "\"C:/Program Files/dotnet/dotnet.exe\" ef database update -c catalogcontext --configuration Release -p \"${workspace}/src/Infrastructure/Infrastructure.csproj\" -s \"${workspace}/src/Web/Web.csproj\""                
                bat "\"C:/Program Files/dotnet/dotnet.exe\" ef database update -c appidentitydbcontext --configuration Release -p \"${workspace}/src/Infrastructure/Infrastructure.csproj\" -s \"${workspace}/src/Web/Web.csproj\""
            }
        }
        stage ('Deploy') {
            steps {
                powershell '''
                    $psw = ConvertTo-SecureString -String $env:APP_SERVER_CREDS_PSW -AsPlainText -Force
                    $cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $env:APP_SERVER_CREDS_USR, $psw -Verbose
                    try
                    {
                        $targetSession = New-PSSession -ComputerName $env:appServerName -Credential $cred -Authentication Basic
                    }
                    catch 
                    {
                        Write-Host "Can't establish remote PowerShell session with app server. Verify app server credential is correct and try again." -ForegroundColor Yellow
                        exit 1
                    }
                    Invoke-Command -Session $targetSession -ScriptBlock {
                        $appName = "eshoponweb"
                        Stop-Service -name was,w3svc -passThru
                    }
                    try 
                    {
                        Write-Host "Copying artifacts to server"
                        Copy-Item -Force -Recurse -ToSession $targetSession -Path \"$env:workspace/publish/*\" \"C:\\inetpub\\eshoponweb\" -ErrorAction Stop
                        Write-Host "Copying complete!"
                    }
                    catch 
                    {
                        Write-Host "Can't copy artifacts to app server. Verify app server connection and try again." -ForegroundColor Yellow
                        exit 1
                    }
                    Write-Host "Configuring IIS on the server"
                    Invoke-Command -Session $targetSession -ScriptBlock { 
                        Start-Service -name was,w3svc -passThru
                        Start-Sleep -Seconds 5
                        Import-Module "WebAdministration"
                        # Create app pool
                        Remove-Website -Name *
                        Remove-WebAppPool -Name *
                        

                        New-Item IIS:\\AppPools\\$appName -Force
                        Set-ItemProperty -Path "IIS:\\AppPools\\${appName}" -name "managedRuntimeVersion" -value ""
                        Set-ItemProperty -Path "IIS:\\AppPools\\${appName}" -name "autoStart" -value $true
                        Set-ItemProperty -Path "IIS:\\AppPools\\${appName}" -name "startMode" -value "AlwaysRunning"

                        Set-ItemProperty -Path "IIS:\\AppPools\\${appName}" -name "processModel" -value @{identitytype="ApplicationPoolIdentity"}

                        $webSitePath = 'C:\\inetpub\\' + $appName

                        # Create new website
                        New-Item IIS:\\Sites\\$appName -Force -physicalPath $webSitePath  -bindings @{protocol="http";bindingInformation="*:80:"}
                        Set-ItemProperty IIS:\\Sites\\$appName -name applicationPool -value $appName

                        iisreset
                    }
                    Write-Host "Configuring Filebeat and MetricBeat"
                    Invoke-Command -Session $targetSession -ScriptBlock {
                        Stop-Service -Name filebeat,metricbeat | Out-Null
                    }
                    try 
                    {
                        Write-Host "Copying Filebeat and Metricbeat configs to server"
                        Copy-Item -Force -ToSession $targetSession -Path \"$env:workspace/filebeat.yml\" \"C:\\ProgramData\\chocolatey\\lib\\filebeat\\tools\\\" -ErrorAction Stop -Verbose
                        Copy-Item -Force -ToSession $targetSession -Path \"$env:workspace/metricbeat.yml\" \"C:\\ProgramData\\chocolatey\\lib\\metricbeat\\tools\\\" -ErrorAction Stop -Verbose
                        Write-Host "Copying complete!"
                    }
                    catch 
                    {
                        $ErrorMessage = $_.Exception.Message
                        Write-Output "Error: $ErrorMessage" 
                        Write-Host "Can't copy artifacts to app server. Verify app server connection and try again." -ForegroundColor Yellow
                        exit 1
                    }
                    Invoke-Command -Session $targetSession -ScriptBlock {
                        Start-Service -Name filebeat,metricbeat -PassThru
                    }
                    Write-Host 'Configuration Complete'
                    Write-Host 'Checking Website'
                    $Url = \"http://$env:appServerName\"
                    $Response = wget $Url -UseBasicParsing
                    Write-Host "Got the following HTTP response code:"
                    Write-Host $Response.StatusCOde
                    if ($Response.StatusCode -ne 200) {
                        Write-Host "Got invalid reponse code from server" -ForegroundColor Yellow
                        exit 1
                    }
                    Get-PSSession | Remove-PSSession
                '''
            }
        }
    }
}